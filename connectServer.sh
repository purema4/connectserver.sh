#!/bin/bash

SERVER_ADD=purema4.ddns.net
SERVER_PORT=22
SERVER_USER=root
LOCAL_MOUNT=/mnt

function help {

echo "SSFH connect to remote server and mapping file system"
echo "Usage: connectServer.sh [OPTIONS]..."
echo "       connectServer.sh -d MOUNT"
echo "OPTIONS:"
echo "-a | --address: DNS or IP address of the remote server"
echo "-u | --user: remote user, default is set to root. you may want to use a non root user instead."
echo "-m | --mount: the local mountpoint of the file system. Default is set to /mnt."
echo "-p | --port: the remote ssh port to use. Default is set to 22 duh."
echo "-d | --disconnect: while disconenct the local mount point from the remote server."
exit
}

function disconnect {

fusermount3 -u $1 && \
echo "mount folder dicsonnected :)" || \
echo "Unable to disconnect file system :("

exit

}

if [ $# -eq 0 ]
  then
    help
fi

while [[ $# -gt 0 ]]
do

key="$1"

case $key in

  -m|--mount)
  LOCAL_MOUNT=$2
  shift
  shift
  ;;

  -p|--port)
  SERVER_PORT=$2
  shift
  shift
  ;;

  -u|--user)
  SERVER_USER=$2
  shift
  shift
  ;;

  -a|--address)
  SERVER_ADD=$2
  shift
  shift
  ;;

  -d|--disconnect)
  disconnect $2
  shift
  ;;

  *)
  help
  shift
  ;;

esac
done


printf "Connecting to %s with username: %s\n" $SERVER_ADD $SERVER_USER

sshfs $SERVER_USER@$SERVER_ADD:/home/$SERVER_USER $LOCAL_MOUNT -C -p $SERVER_PORT && \
echo "Connected succesfuly" || \
echo "Error when connecting"
